.
├── CODE_OF_CONDUCT.md
├── CODE_OF_CONDUCT.txt
├── CODE_OF_CONDUCT_DE.md
├── CODE_OF_CONDUCT_DE.txt
├── LICENSE
├── README.md
├── SECURITY.md
├── SECURITY.rst
├── Vulnerability_Disclosure_Policy.md
├── add_directory_documentation.py
├── create_robots.sh
├── dataset
│   └── placeholder.txt
├── dep
│   └── placeholder.txt
├── documentation
│   ├── robots.txt
│   └── scorecard
│       └── scorecard.md
├── example
│   └── placeholder.txt
├── humans.txt
├── process_scorecard_results.py
├── repo_setup.sh
├── res
│   ├── main
│   │   ├── osv_repo.json
│   │   └── osv_sbom.json
│   ├── master
│   │   ├── osv_repo.json
│   │   └── osv_sbom.json
│   └── placeholder.txt
├── robots.txt
├── security.txt
├── src
│   └── placeholder.txt
├── structure.txt
├── test
│   └── placeholder.txt
└── tools
    └── placeholder.txt

12 directories, 30 files
